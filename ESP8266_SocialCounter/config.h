// Module connection pins (Digital Pins)
#define CLK D0
#define DIO D1
SevenSegmentTM1637 twitter(CLK, DIO);

#define CLK D2
#define DIO D3
SevenSegmentTM1637 youtube(CLK, DIO);

#define CLK D4
#define DIO D5
SevenSegmentTM1637 facebook(CLK, DIO);

#define CLK D6
#define DIO D7
SevenSegmentTM1637 instagram(CLK, DIO);

const int buzzer = D8; // Buzzer Pin

// WiFi Setup
const char* ssid = "SSID";
const char* password = "PASWD";
char* espHostname = "esp-socialcounter01";

// MQTT Server
const char* mqtt_server = "Mqtt.example.de";

// MQTT Last will and Testament
byte willQoS = 0;
const char* willTopic = "lwt/esp-socialcounter01";
const char* willOnMessage = "online";
const char* willOffMessage = "offline";
const char* willClientID = "esp-socialcounter01";
boolean willRetain = true;
