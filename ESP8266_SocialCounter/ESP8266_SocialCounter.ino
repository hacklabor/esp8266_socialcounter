#include "SevenSegmentTM1637.h" //you can get this from here https://github.com/bremme/arduino-tm1637
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "pitches.h"
extern "C" {
#include "user_interface.h" //to set the hostname
}

#include "config.h"

// for loops
int i;
int k;

// to remeber the old values
int twitterOldnumber;
int facebookOldnumber;
int youtubeOldnumber;
int instagramOldnumber;

int number;
String state = "true";

WiFiClient espClient;
PubSubClient client(espClient);


void setup() {

  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  //initialize all displays
  twitter.begin();
  twitter.setBacklight(100);
  twitter.print(1111);
  youtube.begin();
  youtube.setBacklight(100);
  youtube.print(2222);
  facebook.begin();
  facebook.setBacklight(100);
  facebook.print(3333);
  instagram.begin();
  instagram.setBacklight(100);
  instagram.print(4444);


  //initialize buzzer
  pinMode(buzzer, OUTPUT);

}

void setup_wifi() {

  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  wifi_station_set_hostname(espHostname);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println();
  
  if (strcmp(topic, "socialcounter/twitter") == 0)
  {
    //convert payload to int
    number = getNumber(payload, length);

    //display number
    twitter.clear();
    twitter.print(number);

    if (twitterOldnumber != number && state == "true" ) {
      // Play buzzer and let the dots blink to attract some attention
     
      updateDisplay(&twitter, number,twitterOldnumber);
    }

    // remember the old value
    twitterOldnumber = number;
  }
 

  if (strcmp(topic, "socialcounter/youtube") == 0)
  {

    //convert payload to int
    number = getNumber(payload, length);

    //display number
    youtube.clear();
    youtube.print(number);

    if (youtubeOldnumber != number && state == "true") {

      // Play buzzer and let the dots blink to attract some attention
     
      updateDisplay(&youtube, number,youtubeOldnumber);
    }

    // remember the old value
    youtubeOldnumber = number;

  }

  if (strcmp(topic, "socialcounter/facebook") == 0)
  {

    //convert payload to int
    number = getNumber(payload, length);

    //display number
    facebook.clear();
    facebook.print(number);

    if (facebookOldnumber != number && state == "true") {

      // Play buzzer and let the dots blink to attract some attention
      updateDisplay(&facebook, number,facebookOldnumber);
    }

    // remember the old value
    facebookOldnumber = number;

  }

  if (strcmp(topic, "socialcounter/instagram") == 0)
  {

    //convert payload to int
    number = getNumber(payload, length);

    //display number
    instagram.clear();
    instagram.print(number);

    if (instagramOldnumber != number && state == "true") {

      // Play buzzer and let the dots blink to attract some attention
      updateDisplay(&instagram, number,instagramOldnumber);
    }

    // remember the old value
    instagramOldnumber = number;


  }

  if (strcmp(topic, "socialcounter/state") == 0)
  {
    payload[length] = '\0';
    state = String((char*)payload);
    Serial.println(state);
    if (state == "true") {

      twitter.on();
      twitter.print(twitterOldnumber);
      youtube.on();
      youtube.print(youtubeOldnumber);
      facebook.on();
      facebook.print(facebookOldnumber);
      instagram.on();
      instagram.print(instagramOldnumber);
    }
    else if (state == "false") {
      twitter.off();
      youtube.off();
      facebook.off();
      instagram.off();
    }
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      Serial.println("connected");

      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);
      // ... and resubscribe
      client.subscribe("socialcounter/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}


int getNumber(byte * payload, unsigned int length) {

  payload[length] = '\0';
  String counter = String((char*)payload);
  number = counter.toInt();
  return number;
}


void updateDisplay(SevenSegmentTM1637 * pDisplay, int number, int oldNum)
{
  // Play buzzer and let the dots blink to attract some attention

  int  count = 1; //loop $count times through the function
  while (count -- )
  {
    if (number>oldNum){
    tone(buzzer, NOTE_E6, 125);
    delay(130);
    tone(buzzer, NOTE_G6, 125);
    delay(130);
    tone(buzzer, NOTE_E7, 125);
    delay(130);
    tone(buzzer, NOTE_C7, 125);
    delay(130);
    tone(buzzer, NOTE_D7, 125);
    delay(130);
    tone(buzzer, NOTE_G7, 125);
    delay(125);
    noTone(buzzer);
    }
    else
    {
    tone(buzzer, NOTE_G7, 125);
    delay(1000);
    tone(buzzer, NOTE_E7, 125);
    delay(500);
   
    noTone(buzzer);
    }
    pDisplay->blink();// blink display
  }
  }






